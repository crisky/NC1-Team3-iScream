//
//  ContentView.swift
//  NC1-Team3-iScream
//
//  Created by Cris on 20/03/23.
//

import SwiftUI
import AVFoundation

struct ScreamView: View {
    @ObservedObject var recorder = Recorder()
    
    fileprivate func LoudMeter() -> some View {
        return ZStack (alignment: .trailing) {
            RoundedRectangle(cornerRadius: 20)
                .strokeBorder(Color.primary400, lineWidth: 2)
                .background(RoundedRectangle(cornerRadius: 20).fill(Color.primary900))
                .frame(width: 122, height: 87)
                .opacity(0.6).offset(x: 14)
            VStack(alignment: .trailing) {
                Text("Loud-o-meter")
                    .foregroundColor(.primary200)
                    .font(.system(size: 11))
                Text(String(format: "%.2f dB", RecordingResults.decibelLevel))
                    .foregroundColor(.white)
                    .font(.system(size: 20))
                    .fontWeight(.semibold)
            }.padding(.trailing, 6)
        }
    }
    
    var body: some View {
        VStack {
            ZStack(alignment: .top) {
                
                let fillLevel = RecordingResults.decibelLevel * 1000.0/RecordingResults.maxDecibelLevel

                Image("Cone").padding(.top, 240)
                Image("Cone Fill").padding(.top, 240)
                    .mask(Rectangle()
                        .frame(height: CGFloat(fillLevel))
                        .offset(y: 300))

                VStack{
                    Spacer()
                    HStack{
                        Spacer()
                        LoudMeter()
                    }
                }
            }.frame(height: 450)

            Spacer()
            Text("Scream as loud as you can!")
                .foregroundColor(.secondary100)
                .font(.system(size: 17))
            Text("There will be a prize for the 1st rank :)")
                .foregroundColor(.secondary400)
                .font(.system(size: 12))
        }.padding(.bottom, 83)
    }
}

struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        ScreamView()
    }
}

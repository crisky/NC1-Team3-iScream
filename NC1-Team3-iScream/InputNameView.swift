//
//  InputNameView.swift
//  NC1-Team3-iScream
//
//  Created by Ardli Fadhillah on 25/03/23.
//

import SwiftUI

struct InputNameView: View {
    @Environment(\.dismiss) var dismiss
    @ObservedObject var users: Users
    @Binding var currentUser: User
    
    @State private var userName = ""
    @FocusState private var focusedField: Int?
    
    func GetRankNumber() -> Int {
        var i = 0
        let newUserScore = Double( RecordingResults.highestDecibelLevel)
        for user in users.data {
            if user.scoreDecibel < newUserScore {
               break
            }
            i += 1
        }
        return i
    }
    
    func GetRankMessage(for rank: Int) -> String {
        let i = rank + 1
        
        if i == 1 {
            return "loudest"
        } else if i == 2 {
            return "second loudest"
        } else if i == 3 {
            return "third loudest"
        } else {
            return "\(i)th loudest"
        }
    }
    
    var body: some View {
        ZStack {
            Color.primary800
            
            VStack() {
                Text(String(format: "Scream Lvl: %.2f Db", RecordingResults.highestDecibelLevel))
                    .font(.system(size: 28))
                Divider()
                
                let rank = GetRankNumber()
                
                Text("Your scream is the " + GetRankMessage(for: rank) + " in the leaderboard! Enter your name to save your record, or go back to retry your scream: ")
                    .font(.system(size: 14))
                
                HStack {
                    Text("Name")
                        .padding(.leading, 8)
                    TextField("e.g John", text: $userName)
                        .foregroundColor(.white)
                        .preferredColorScheme(.dark)
                        .frame(height: 44)
                        .padding(.horizontal, 16)
                        .background(Color.primary900)
                        .textInputAutocapitalization(.never)
                        .disableAutocorrection(true)
                        .focused($focusedField, equals: 0)
                        .onAppear(perform: { focusedField = 0 })
                }
                .background(Color.primary900)
                
                Spacer()
                
                Button(action: {
                    dismiss()
                }) {
                    Text("Retry Your Scream")
                        .foregroundColor(.white)
                        .font(.system(size: 17))
                        .frame(height: 50)
                        .frame(maxWidth: .infinity)
                        .padding(.horizontal, 16)
                }
                .background(Color.primary600)
                .cornerRadius(12)
                .padding(.top, 24)
                
                Button(action: {
                    currentUser = User(nickName: userName, scoreDecibel: Double(RecordingResults.highestDecibelLevel))
                    users.insertData(newUser: currentUser, at: rank)
                    dismiss()
                }) {
                    Text("Insert To Leaderboard")
                        .foregroundColor(.white)
                        .font(.system(size: 17))
                        .frame(height: 50)
                        .frame(maxWidth: .infinity)
                        .padding(.horizontal, 16)
                }
                .background(Color.secondary500)
                .cornerRadius(12)
                .padding(.top, 8)
            }
            .padding(.horizontal, 16)
            .padding(.vertical, 32)
        }
    }
}

struct InputNameView_Previews: PreviewProvider {
    static var previews: some View {
        InputNameView(users: Users(), currentUser: .constant(User(nickName: "", scoreDecibel: 0.0)))
    }
}

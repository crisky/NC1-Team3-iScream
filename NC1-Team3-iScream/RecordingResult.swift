//
//  RecordingResult.swift
//  NC1-Team3-iScream
//
//  Created by Ardli Fadhillah on 24/03/23.
//

import Foundation

class RecordingResults {
    static var decibelLevel: Float = 0
    static var highestDecibelLevel: Float = 0
    static var maxDecibelLevel: Float = 200.0
}

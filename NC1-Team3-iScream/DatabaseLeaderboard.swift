//
//  DatabaseLeaderboard.swift
//  NC1-Team3-iScream
//
//  Created by M Irsyad R on 24/03/23.
//

import Foundation

struct LeaderboardData : Codable {
    let records: [LeaderboardDataResponse]?
}

struct LeaderboardDataResponse: Codable {
    let id: String?
    let fields: LeaderboardDataFieldResponse?
    let createdTime: String?
}

struct LeaderboardDataFieldResponse: Codable {
    let nickName: String?
    let scoreDecibel: Int?
}

//Mock Database
struct User : Identifiable {
    let id = UUID()
    let nickName: String
    let scoreDecibel: Double
}
class Users: ObservableObject {
    @Published var data: [User] = [User(nickName: "Jokho", scoreDecibel: 20.0)]
    
    func insertData(newUser: User, at index: Int) {
        self.data.insert(newUser, at: index)
    }
}

//To integrate Airtable database with your SwiftUI app, you will need to use Airtable's REST API. Here's a general outline of the steps involved:
//
//First, you will need to create an Airtable account and create a base with the desired tables and fields to store your data.
//
//Next, you will need to obtain an API key from Airtable. You can do this by logging into your account, navigating to your account settings, and generating an API key for your account.
//
//In your SwiftUI app, you can use the URLSession API to make HTTP requests to the Airtable API. You will need to include your API key in the request headers and specify the appropriate API endpoint for the action you want to perform (e.g., creating a record, updating a record, retrieving records, etc.).
//
//You can use the Codable protocol in Swift to map the JSON response from the Airtable API to Swift structs or classes in your app. This will allow you to easily work with the data in your app.
//
//Here's an example of how you could use the Airtable API to retrieve records from a table:

//struct ContentView: View {
//    let airtableAPIKey = "YOUR_API_KEY"
//    let airtableBaseID = "YOUR_BASE_ID"
//    let airtableTableName = "YOUR_TABLE_NAME"
//
//    @State var records: [AirtableRecord] = []
//
//    var body: some View {
//        List(records, id: \.id) { record in
//            Text(record.fields["Name"] as? String ?? "Unknown")
//            Text(record.fields["Email"] as? String ?? "Unknown")
//        }
//        .onAppear {
//            let url = URL(string: "https://api.airtable.com/v0/\(airtableBaseID)/\(airtableTableName)")!
//            var request = URLRequest(url: url)
//            request.addValue("Bearer \(airtableAPIKey)", forHTTPHeaderField: "Authorization")
//
//            URLSession.shared.dataTask(with: request) { data, response, error in
//                if let data = data {
//                    do {
//                        let decoder = JSONDecoder()
//                        let airtableResponse = try decoder.decode(AirtableResponse.self, from: data)
//                        self.records = airtableResponse.records
//                    } catch {
//                        print("Error decoding Airtable response: \(error)")
//                    }
//                } else if let error = error {
//                    print("Error retrieving Airtable records: \(error)")
//                }
//            }.resume()
//        }
//    }
//}
//
//struct AirtableResponse: Codable {
//    let records: [AirtableRecord]
//}
//
//struct AirtableRecord: Codable, Identifiable {
//    let id: String
//    let fields: [String: Any]
//}

//This example assumes that you have a AirtableRecord struct that represents a record in your Airtable table, and that your Airtable API key, base ID, and table name are stored in constants. In the onAppear closure, the app makes an HTTP request to retrieve records from the specified table, using the URLSession API and the Codable protocol to decode the response into an array of AirtableRecord instances. The retrieved records are stored in the records array, which is displayed in a List view.

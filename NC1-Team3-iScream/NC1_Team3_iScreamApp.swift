//
//  NC1_Team3_iScreamApp.swift
//  NC1-Team3-iScream
//
//  Created by Cris on 20/03/23.
//

import SwiftUI

@main
struct NC1_Team3_iScreamApp: App {
    @StateObject var recorder = Recorder()
    
    var body: some Scene {
        WindowGroup {
            MainView()
        }
    }
}

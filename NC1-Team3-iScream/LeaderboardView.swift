//
//  LeaderboardView.swift
//  NC1-Team3-iScream
//
//  Created by Ardli Fadhillah on 22/03/23.
//

import SwiftUI

struct LeaderboardView: View {
    @ObservedObject var users: Users
    var body: some View {
        HStack {
            VStack {
                List(users.data.enumerated().map({$0}), id: \.element.id) { index, user in
                    Text("\(index+1). \(user.nickName) - " + String(format: "%.2lf Db", user.scoreDecibel))
                        .foregroundColor(Color.white)
                        .listRowBackground(Color.primary800)
                        .listRowSeparatorTint(Color.black.opacity(0.3))
                }
                .scrollContentBackground(.hidden)
            }
            .frame(maxWidth: .infinity)
            .padding(.top, 240.0)
            .padding(.bottom, 24.0)
            .border(Color.primary400, width: 1)
            .offset(y:4)
        }
        .background(Color.primary900)
        .frame(maxWidth: .infinity)
        .padding(.horizontal, 16.0)
    }
}

struct LeaderboardView_Previews: PreviewProvider {
    static var previews: some View {
        LeaderboardView(users: Users())
    }
}

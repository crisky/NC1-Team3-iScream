//
//  ColorPallete.swift
//  NC1-Team3-iScream
//
//  Created by Ardli Fadhillah on 22/03/23.
//

import Foundation
import SwiftUI

extension Color {
    static let primary200 = Color(red: 217.0/255.0, green: 214.0/255.0, blue: 254.0/255.0)
    static let primary400 = Color(red: 155.0/255.0, green: 138.0/255.0, blue: 251.0/255.0)
    static let primary500 = Color(red: 122.0/255.0, green: 90.0/255.0, blue: 248.0/255.0)
    static let primary600 = Color(red: 105.0/255.0, green: 56.0/255.0, blue: 239.0/255.0)
    static let primary800 = Color(red: 74.0/255.0, green: 31.0/255.0, blue: 184.0/255.0)
    static let primary900 = Color(red: 62.0/255.0, green: 28.0/255.0, blue: 150.0/255.0)
    static let secondary100 = Color(red: 251.0/255.0, green: 232.0/255.0, blue: 255.0/255.0)
    static let secondary400 = Color(red: 228.0/255.0, green: 120.0/255.0, blue: 250.0/255.0)
    static let secondary500 = Color(red: 212.0/255.0, green: 68.0/255.0, blue: 241.0/255.0)
}


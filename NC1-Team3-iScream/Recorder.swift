//
//  Recorder.swift
//  TrialVoice2
//
//  Created by Cris on 23/03/23.
//

import AVFoundation

class Recorder: NSObject, ObservableObject {
    private let audioSession = AVAudioSession.sharedInstance()
    private var audioRecorder: AVAudioRecorder?
    private var levelTimer: Timer?

    @Published var isRecording = false
    @Published var decibelLevel: Float = 0.0

    override init() {
        super.init()

        // Set up audio session
        do {
            try audioSession.setCategory(.record, mode: .measurement, options: .duckOthers)
            try audioSession.setActive(true)
            audioSession.requestRecordPermission { (allowed) in
                DispatchQueue.main.async {
                    if allowed {
//                        print("Microphone permission granted.")
                    } else {
//                        print("Microphone permission denied.")
                    }
                }
            }
        } catch {
            print("Failed to set up audio session.")
        }
    }

    func toggleRecording() {
        if isRecording {
            stopRecording()
        } else {
            print("test")
            startRecording()
        }
    }

    private func startRecording() {
        let recordingFilename = getDocumentsDirectory().appendingPathComponent("recording.wav")
        let recordingSettings: [String: Any] = [
            AVFormatIDKey: Int(kAudioFormatLinearPCM),
            AVSampleRateKey: 44100,
            AVNumberOfChannelsKey: 1,
            AVEncoderAudioQualityKey: AVAudioQuality.high.rawValue
        ]

        do {
            audioRecorder = try AVAudioRecorder(url: recordingFilename, settings: recordingSettings)
            audioRecorder?.prepareToRecord()
            audioRecorder?.record()
            audioRecorder?.isMeteringEnabled = true

            levelTimer = Timer.scheduledTimer(withTimeInterval: 0.1, repeats: true, block: { [unowned self] (timer) in
                audioRecorder?.updateMeters()
                let decibels = audioRecorder?.averagePower(forChannel: 0) ?? -160.0
                let level: Float
                let minDecibels: Float = -80.0
                if decibels < minDecibels {
                    level = 0.0
                } else if decibels >= 10.0 {
                    level = 1.0
                } else {
                    let root: Float = 2.0
                    let minAmp: Float = powf(10.0, 0.05 * minDecibels)
                    let inverseAmpRange: Float = 1.0 / (1.0 - minAmp)
                    let amp: Float = powf(10.0, 0.05 * decibels)
                    let adjAmp: Float = (amp - minAmp) * inverseAmpRange
                    level = powf(adjAmp, 1.0 / root)
                }
                decibelLevel = level * 120.0 + 20
                RecordingResults.decibelLevel = decibelLevel
                print("Decibel level updated to: \(RecordingResults.decibelLevel) dB")
                print(decibels)
                
                if decibelLevel > RecordingResults.highestDecibelLevel {
                    RecordingResults.highestDecibelLevel = decibelLevel
                }
            })

            isRecording = true
            print("Successfully recording")
        } catch {
            print("Failed to start recording.")
        }
    }

    func stopRecording() {
        audioRecorder?.stop()
        audioRecorder = nil
        levelTimer?.invalidate()
        levelTimer = nil
        isRecording = false
        RecordingResults.decibelLevel = 0
    }

    private func getDocumentsDirectory() -> URL {
        let paths = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask)
        return paths[0]
    }
}


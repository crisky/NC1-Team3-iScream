//
//  MainView.swift
//  NC1-Team3-iScream
//
//  Created by Ardli Fadhillah on 22/03/23.
//

import SwiftUI

struct MainView: View {
    
    @ObservedObject var recorder = Recorder()
    @ObservedObject var users = Users()
    @State var selectedTab = 0
    @State private var isRecording = false
    
    @State private var effect1 = false
    @State private var effect2 = false
    
    @State private var inputNameViewActive = false
    @State private var currentUser = User(nickName: "", scoreDecibel: 0.0)
    
    init() {
        UITabBar.appearance().backgroundColor = UIColor(Color.primary800)
        UITabBar.appearance().unselectedItemTintColor = UIColor(Color.primary500)
    }
    
    var body: some View {
        
        ZStack {
            TabView(selection: $selectedTab) {
                ZStack {
                    Background()
                    ScreamView()
                }
                .tabItem {
                    Label("iScream", systemImage: "house.fill")
                }.tag(0)

                ZStack {
                    Background()
                    LeaderboardView(users: users)
                }
                .tabItem {
                    Label("Leaderboard", systemImage: "list.dash")
                }.tag(1)
            }
            .disabled(isRecording)

            if selectedTab == 0 {
                Header(title: "Screaammm!!")
                VStack {
                    Spacer()
                    RecordBtn()
                }
            }
            else if selectedTab == 1 {
                Header(title: "Leaderboard")
            }
        }.sheet(isPresented: $inputNameViewActive, content: { InputNameView(users: users, currentUser: $currentUser) })
    }
    
    fileprivate func Header(title: String) -> some View {
        return VStack {
            ZStack(alignment: .top) {
                Image("Cream").ignoresSafeArea()
                Text(title)
                    .foregroundColor(.white)
                    .font(.system(size: 34))
                    .padding(.top, 80)
                    .fontWeight(.bold)
                    .ignoresSafeArea()
            }
            Spacer()
        }
    }
    
    fileprivate func Background() -> some View {
        return Color.clear.background(Image("BG")
            .resizable()
            .ignoresSafeArea()
            .scaledToFill())
    }
    
    fileprivate func RecordBtn() -> some View {
        return ZStack {
            Circle().strokeBorder(Color.secondary400, lineWidth: 1).background(Circle().fill(Color.primary900)).frame(width: 90, height: 90)
            
            Circle()
                .frame(width: 50, height: 50)
                .foregroundColor(Color.secondary500)
                .scaleEffect(effect1 ? 1 : 1.2)
                .animation(.easeInOut(duration: 0.4).repeatForever(), value: effect1)
                .onAppear(){
                    self.effect1.toggle()
                }
            
            Button(action: {
                isRecording.toggle()
                recorder.toggleRecording()
                
                if !isRecording {
                    inputNameViewActive = true
                } else {
                    RecordingResults.highestDecibelLevel = 0
                }
            }) {
                Image(systemName: !isRecording ? "mic.fill" : "stop.fill")
                    .foregroundColor(.white).frame(width: 50, height: 50)
            }.onAppear(perform: {
                isRecording = false
                recorder.stopRecording()
            })
        }
    }
}

struct MainView_Previews: PreviewProvider {
    static var previews: some View {
        MainView().previewDevice("iPhone 14 Pro")
    }
}
